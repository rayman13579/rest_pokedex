package at.robben.rest_pokedex.dao;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import at.robben.rest_pokedex.vo.Pokemon;

import java.sql.Connection;

public class PokemonDAO {
	public Connection connection;

	public PokemonDAO() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String URL = "jdbc:mysql://localhost/pokedex_web";
			String USER = "root";
			String PASS = "";
			this.connection = DriverManager.getConnection(URL, USER, PASS);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			System.out.println("Error");
		}
	}

	public List<Pokemon> getAllPokemon() {
		List<Pokemon> PokemonList = new ArrayList<>();
		try {
			Statement st = this.connection.createStatement();
			ResultSet rs = st.executeQuery("Select * from pokemon");
			rs.first();

			while (!rs.isAfterLast()) {
				Pokemon p = new Pokemon(rs.getInt("id"), rs.getString("name"), rs.getString("type"));
				PokemonList.add(p);
				rs.next();
			}
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return PokemonList;
	}

	public List<Pokemon> getPokemonById(int id) {
		List<Pokemon> PokemonList = new ArrayList<>();
		try {
			Statement st = this.connection.createStatement();
			ResultSet rs = st.executeQuery("Select * from pokemon where id LIKE '" + id + "%'");
			rs.first();

			while (!rs.isAfterLast()) {
				Pokemon p = new Pokemon(rs.getInt("id"), rs.getString("name"), rs.getString("type"));
				PokemonList.add(p);
				rs.next();
			}
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return PokemonList;
	}

	public boolean addPokemon(Pokemon pokemon) {
		boolean ok = false;
		try {
			Statement st = this.connection.createStatement();
			ok = st.execute("Insert into pokemon values('" + pokemon.getId() + "','" + pokemon.getName()+ "','" + pokemon.getType() + "')");
		} catch (SQLException e) {
			e.printStackTrace();
			ok = false;
			return ok;
		}
		ok = true;
		return ok;
	}
	
	public boolean deletePokemon(int id) {
		boolean ok = false;
		try {
			Statement st = this.connection.createStatement();
			ok = st.execute("Delete from pokemon where id="+id+"");
		} catch (SQLException e) {
			e.printStackTrace();
			ok = false;
			return ok;
		}
		ok = true;
		return ok;
	}

	public static void main(String[] args) {
		PokemonDAO dao = new PokemonDAO();
		List<Pokemon> PokemonList = dao.getAllPokemon();
		for (Pokemon pokemon : PokemonList) {
			System.out.println(pokemon.getName());
		}
	}
}