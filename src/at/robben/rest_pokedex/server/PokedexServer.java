package at.robben.rest_pokedex.server;

import java.io.IOException;

import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.net.httpserver.HttpServer;

public class PokedexServer {

	public static void main(String[] args) throws IllegalArgumentException, IOException {
		ResourceConfig rc = new PackagesResourceConfig("");
	      rc.getProperties().put(
	          "com.sun.jersey.spi.container.ContainerResponseFilters",
	          "at.robben.rest_pokedex.server.services.filters.CORSFilter"
	      );
	      HttpServer server = HttpServerFactory.create("http://localhost:8081/", rc);
	      server.start();
	}
}