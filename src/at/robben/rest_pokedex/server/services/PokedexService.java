package at.robben.rest_pokedex.server.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import at.robben.rest_pokedex.dao.PokemonDAO;
import at.robben.rest_pokedex.vo.Pokemon;

@Path("/pokedex")
public class PokedexService {
  
  
  @Path("")
  @GET
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public List<Pokemon> searchPokemon(@QueryParam("filter") String filter){
    PokemonDAO dao = new PokemonDAO();
    return dao.getAllPokemon();
  }

  @Path("/{id}")
  @GET
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public List<Pokemon> getPokemonByID(@PathParam("id") int id) {
    PokemonDAO dao = new PokemonDAO();
    return dao.getPokemonById(id);
  }
  
  
  
  @Path("")
  @POST
  @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
  public Response addPokemon(Pokemon pokemon){
    PokemonDAO dao = new PokemonDAO();
    boolean ok = dao.addPokemon(pokemon);
    if (ok == true){
      return Response.ok().build();
    }else{
      return Response.noContent().build();
    }
  }
  
  @Path("/{id}")
  @POST
  @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
  public Response delete(@PathParam("id") int id){
    PokemonDAO dao = new PokemonDAO();
    boolean ok = dao.deletePokemon(id);
    if (ok == true){
      return Response.ok().build();
    }else{
      return Response.noContent().build();
    }
  }
  
}