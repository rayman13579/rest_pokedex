package at.robben.rest_pokedex.vo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pokemon {
	private int id;
	private String name;
	private String type;
	
	public Pokemon() {
		// TODO Auto-generated constructor stub
	}

	public Pokemon(int id, String name, String type) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	

}